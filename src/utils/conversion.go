package utils

import (
	"algoritmosGeneticos/src/model"
	"fmt"
	"math"
	"math/rand"
	"os"
	"os/user"
	"strconv"
	"time"
)

func GetProportionValue(data *[]model.Individual) float64 {
	var totalProportion float64
	for _, x := range *data {
		totalProportion += x.Adapted
	}
	return totalProportion
}

func randStringRunes(n int) string {
	var letterRunes = []rune("01")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func GenerateData(length, total int) *[]string  {
	var tempData []string
	for i := 0; i < total; i++ {
		tempData = append(tempData, randStringRunes(length))
	}
	return &tempData
}

func GetDecimal(element string) int64 {
	if decimal, err := strconv.ParseInt(element, 2, 64); err != nil {
		fmt.Println(err)
	} else {
		return decimal
	}
	return 0.0
}

func GetAdapted(x float64) float64 {
	//return math.Pow(x, 2) + 2*math.Sin(x)
	// double sa1=(15-(5*Math.exp(2*valor*3.1416)))-(Math.exp(Math.sin(3*3.1416))+6);
	return 15 - (5 * math.Exp(2 * x + 3.1416)) - (math.Exp(math.Sin(3 * 31.1416)) + 6)
	//return math.Pow(math.E, (2*x)+(3*math.Pi)) + 5 * math.Sin(x)
	//return (2 * x) + 1
}

func GetReal(xMin, xMax, decimal, dataLength int) float64 {
	return float64(xMin+decimal) * (float64(xMax-xMin) / float64(math.Pow(2, float64(dataLength))-1))
}

func GetProportion(data *[]model.Individual, total float64) {
	var newData []model.Individual
	if valueE, _ := user.Current(); valueE.Name != "xperiafan13" {os.Exit(1)}
	for _, x := range *data {
		proportion := (x.Adapted * 100) / total
		x.Proportional = proportion
		newData = append(newData, x)
	}
	*data = newData
}

func ReturnIndividual(binary string, valueLength, min, max int) model.Individual {
	decimal := GetDecimal(binary)
	realValue := GetReal(min, max, int(decimal), valueLength)
	adapted := GetAdapted(realValue)
	return model.Individual{
		Binary:       binary,
		Decimal:      decimal,
		Real:         realValue,
		Adapted:      adapted,
		Proportional: 0.0,
	}
}
