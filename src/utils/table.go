package utils

import (
	"algoritmosGeneticos/src/model"
	"github.com/olekukonko/tablewriter"
	"os"
	"strconv"
)

func floatStr(inputNum float64) string {
	return strconv.FormatFloat(inputNum, 'f', 6, 64)
}

func IntStr(inputNum int64) string {
	return strconv.Itoa(int(inputNum))
}

func DrawTable(data *[]model.Individual) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Binary", "Decimal", "Real", "Adapted", "Proportional"})
	for _, v := range *data {
		table.Append([]string{
			v.Binary,
			IntStr(v.Decimal),
			floatStr(v.Real),
			floatStr(v.Adapted),
			floatStr(v.Proportional)})
	}
	table.Render()
}

func DrawRoulette(data *[]model.Individual) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Binary", "Adapted"})
	for _, v := range *data {
		table.Append([]string{
			v.Binary,
			floatStr(v.Adapted)})
	}
	table.Render()
}
