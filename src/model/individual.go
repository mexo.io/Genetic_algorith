package model

type Individual struct {
	Binary       string
	Decimal      int64
	Real         float64
	Adapted      float64
	Proportional float64
}

type Description struct {
	Method      string
	Tournament  bool
	Elitist		bool
	CrossOne    int
	CrossTwo    int
	XMin        int
	XMax        int
	Convergence int
	Length      float64
	Crossing    int
	Total       int
}
