package elitist

import (
	"algoritmosGeneticos/src/model"
	"github.com/pmylund/sortutil"
)

func Elitist(data *[]model.Individual) *model.Individual {
	sortutil.DescByField(*data, "Adapted")
	*data = remove(data)
	return &(*data)[0]
}

func remove(data *[]model.Individual) []model.Individual {
	return (*data)[:len(*data) - 1]
}
