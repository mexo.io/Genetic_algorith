package process

import (
	"algoritmosGeneticos/src/crosses"
	. "algoritmosGeneticos/src/model"
	"algoritmosGeneticos/src/utils"
	"fmt"
	"github.com/alok87/goutils/pkg/random"
	"reflect"
)

type Process struct {
	*GenericDAO
}

func NewProcess(con *[]Individual, des *Description) *Process {
	return &Process{NewGeneric(con, des)}
}

func (ind *Process) random(min, max int) []int {
	arr := random.RangeInt(min, max, 2)
	return arr
}

func (ind *Process) getStrong(data []Individual) Individual {
	if data[0].Adapted < data[1].Adapted {
		return data[1]
	} else {
		return data[0]
	}
}

func (ind *Process) getWeak(data []Individual) Individual {
	if data[0].Adapted < data[1].Adapted {
		return data[0]
	} else {
		return data[1]
	}
}

func (ind *Process) do(attempt int) *[]Individual {
	var individuals = *ind.Individual
	var sons []Individual
	var temp = ind.random(0, len(individuals))
	fatherOne := individuals[temp[0]]
	fatherTwo := individuals[temp[1]]
	var sonOne, sonTwo Individual
	switch ind.Description.Method {
	case "one_point":
		point := ind.Description.CrossOne
		sonO, sonT := crosses.OnePoint([]string{fatherOne.Binary, fatherTwo.Binary}, point)
		sonOne = utils.ReturnIndividual(sonO, len(sonO), ind.Description.XMin, ind.Description.XMax)
		sonTwo = utils.ReturnIndividual(sonT, len(sonT), ind.Description.XMin, ind.Description.XMax)
	case "two_point":
		pointOne := ind.Description.CrossOne
		pointTwo := ind.Description.CrossTwo
		sonO, sonT := crosses.TwoPoint([]string{fatherOne.Binary, fatherTwo.Binary}, []int{pointOne, pointTwo})
		sonOne = utils.ReturnIndividual(sonO, len(sonO), ind.Description.XMin, ind.Description.XMax)
		sonTwo = utils.ReturnIndividual(sonT, len(sonT), ind.Description.XMin, ind.Description.XMax)
	case "uniform":
		sonO, sonT := crosses.Uniform([]string{fatherOne.Binary, fatherTwo.Binary})
		sonOne = utils.ReturnIndividual(sonO, len(sonO), ind.Description.XMin, ind.Description.XMax)
		sonTwo = utils.ReturnIndividual(sonT, len(sonT), ind.Description.XMin, ind.Description.XMax)
	}
	sons = append(sons, sonOne, sonTwo)
	utils.GetProportion(&sons, utils.GetProportionValue(&individuals))
	strongSon := ind.getStrong(sons)
	weakFather := ind.getWeak([]Individual{fatherOne, fatherTwo})
	for position, element := range individuals {
		if reflect.DeepEqual(weakFather, element) {
			individuals[position] = strongSon
		}
	}
	fmt.Println(fmt.Printf("******** Attempt %d ********", attempt))
	utils.DrawRoulette(&individuals)
	return &individuals
}

func (ind *Process) Init() *[]Individual {
	var individuals []Individual
	for i := 0; i <= ind.Description.Crossing; i++ {
		individuals = *ind.do(i)
	}
	return &individuals
}
