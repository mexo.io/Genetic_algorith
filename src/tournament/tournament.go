package tournament

import (
	"algoritmosGeneticos/src/model"
	"algoritmosGeneticos/src/utils"
	"fmt"
	"github.com/alok87/goutils/pkg/random"
	"math/rand"
	"time"
)

var letterRunes = []rune("01")

func randStringRunes(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func randomNumber(max int) []int {
	arr := random.RangeInt(0, max, 2)
	return arr
}

func Order(length, XMin, XMax, population int) *[]model.Individual {
	var tempData []model.Individual
	var finalData []model.Individual
	for i := 0; i < population + 500; i++ {
		tempValue := randStringRunes(length)
		tempData = append(tempData,
			utils.ReturnIndividual(tempValue, len(tempValue), XMin, XMax))
	}
	fmt.Println(tempData)
	fmt.Println("Tamaño inicial", len(tempData))
	utils.GetProportion(&tempData, utils.GetProportionValue(&tempData))
	for len(finalData) < population {
		tempNumber := randomNumber(len(tempData))
		itemOne := tempData[tempNumber[0]]
		itemTwo := tempData[tempNumber[1]]
		if itemOne.Adapted < itemTwo.Adapted {
			finalData = append(finalData, itemTwo)
		} else {
			finalData = append(finalData, itemOne)
		}
	}
	fmt.Println(tempData)
	fmt.Println("Tamaño final", len(finalData))
	return &finalData
}
