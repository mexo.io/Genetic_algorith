package crosses

import (
	"fmt"
	"math/rand"
)

func OnePoint(value []string, position int) (string, string) {
	one := []string{value[0][0 : len(value[0])-position], value[0][len(value[0])-position : len(value[0])]}
	two := []string{value[1][0 : len(value[1])-position], value[1][len(value[1])-position : len(value[1])]}
	return one[0] + two[1], two[0] + one[1]
}

func TwoPoint(value []string, indices []int) (string, string) {
	oneOne := value[0][0: len(value[0])-indices[1]]
	twoOne := value[0][len(oneOne): len(value[0]) - indices[0]]
	threeOne := value[0][len(twoOne) + len(oneOne):]
	oneTwo := value[1][0: len(value[0])-indices[1]]
	twoTwo := value[1][len(oneTwo): len(value[1]) - indices[0]]
	threeTwo:= value[1][len(twoTwo) + len(oneTwo):]
	return oneOne + twoTwo + threeOne, oneTwo + twoOne + threeTwo
}

var letterRunes = []rune("01")
func randString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func Uniform(value []string) (string, string)  {
	mask := randString(len(value[0]))
	fmt.Println("Mask", mask)
	var oneSon string
	var twoSon string
	for index, char := range mask {
		if char == '0' {
			oneSon+=string(value[0][index])
		} else {
			oneSon+=string(value[1][index])
		}
	}
	for index, char := range mask {
		if char == '1' {
			twoSon+=string(value[0][index])
		} else {
			twoSon+=string(value[1][index])
		}
	}
	return oneSon, twoSon
}