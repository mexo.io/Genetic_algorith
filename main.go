package main

import (
	"algoritmosGeneticos/src/convergence"
	"algoritmosGeneticos/src/model"
	"math"
)

func getCrossing() int {
	return int((65 * 1000) / 100) // Tasa de curce = 65
}

func Init() {
	length := math.Log2(1 + (32-1)/0.00001)
	convergence.NewConvergence(&model.Description{
		Total:       1000,
		Method:      "one_point",
		CrossOne:    2,
		CrossTwo:    4,
		XMin:        1,
		XMax:        32,
		Convergence: 100,
		Tournament:  false,
		Elitist:     false,
		Length:      length,
		Crossing:    getCrossing()}).Do()
}

func main() {
	Init()
}
